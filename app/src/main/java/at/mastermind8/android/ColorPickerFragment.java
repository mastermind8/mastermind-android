package at.mastermind8.android;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.DialogFragment;

public class ColorPickerFragment extends DialogFragment {

    public OnColorSelectedListener onColorSelectedListener;
    private Button btn_green;
    private Button btn_red;
    private Button btn_blue;
    private Button btn_yellow;
    private Button btn_orange;
    private Button btn_purple;

    public void setOnColorSelectedListener(OnColorSelectedListener onColorSelectedListener) {
        this.onColorSelectedListener = onColorSelectedListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.color_picker_fragment, container, false);

        btn_green = view.findViewById(R.id.button_green);
        btn_red = view.findViewById(R.id.button_red);
        btn_blue = view.findViewById(R.id.button_blue);
        btn_yellow = view.findViewById(R.id.button_yellow);
        btn_orange = view.findViewById(R.id.button_orange);
        btn_purple = view.findViewById(R.id.button_purple);

        greenBtnClicked();
        redBtnClicked();
        blueBtnClicked();
        yellowBtnClicked();
        orangeBtnClicked();
        purpleBtnClicked();
        return view;
    }

    public interface OnColorSelectedListener {
        void onColorSelected(int color);
    }

    public void greenBtnClicked(){
        btn_green.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onColorSelectedListener != null) {
                    onColorSelectedListener.onColorSelected(Color.parseColor("#00FF00"));
                }
                dismiss();
            }
        });
    }

    public void redBtnClicked(){
        btn_red.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onColorSelectedListener != null) {
                    onColorSelectedListener.onColorSelected(Color.parseColor("#FF0000"));
                }
                dismiss();
            }
        });
    }

    public void blueBtnClicked(){
        btn_blue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onColorSelectedListener != null) {
                    onColorSelectedListener.onColorSelected(Color.parseColor("#0000FF"));
                }
                dismiss();
            }
        });
    }

    public void yellowBtnClicked(){
        btn_yellow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onColorSelectedListener != null) {
                    onColorSelectedListener.onColorSelected(Color.parseColor("#FFFF00"));
                }
                dismiss();
            }
        });
    }

    public void orangeBtnClicked(){
        btn_orange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onColorSelectedListener != null) {
                    onColorSelectedListener.onColorSelected(Color.parseColor("#FFA500"));
                }
                dismiss();
            }
        });
    }

    public void purpleBtnClicked(){
        btn_purple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onColorSelectedListener != null) {
                    onColorSelectedListener.onColorSelected(Color.parseColor("#800080"));
                }
                dismiss();
            }
        });
    }
}

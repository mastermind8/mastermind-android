package at.mastermind8.android;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class GameActivity extends AppCompatActivity implements ColorPickerFragment.OnColorSelectedListener
{
    FloatingActionButton button_game1;
    FloatingActionButton button_game2;
    FloatingActionButton button_game3;
    FloatingActionButton button_game4;
    FloatingActionButton button_game5;
    FloatingActionButton button_game6;
    FloatingActionButton button_game7;
    FloatingActionButton button_game8;
    FloatingActionButton button_game9;
    FloatingActionButton button_game10;
    FloatingActionButton button_game11;
    FloatingActionButton button_game12;
    FloatingActionButton button_game13;
    FloatingActionButton button_game14;
    FloatingActionButton button_game15;
    FloatingActionButton button_game16;
    FloatingActionButton button_game17;
    FloatingActionButton button_game18;
    FloatingActionButton button_game19;
    FloatingActionButton button_game20;
    FloatingActionButton button_game21;
    FloatingActionButton button_game22;
    FloatingActionButton button_game23;
    FloatingActionButton button_game24;
    FloatingActionButton button_game25;
    FloatingActionButton button_game26;
    FloatingActionButton button_game27;
    FloatingActionButton button_game28;
    FloatingActionButton button_game29;
    FloatingActionButton button_game30;
    FloatingActionButton button_game31;
    FloatingActionButton button_game32;

    FloatingActionButton hint_button_game1;
    FloatingActionButton hint_button_game2;
    FloatingActionButton hint_button_game3;
    FloatingActionButton hint_button_game4;
    FloatingActionButton hint_button_game5;
    FloatingActionButton hint_button_game6;
    FloatingActionButton hint_button_game7;
    FloatingActionButton hint_button_game8;
    FloatingActionButton hint_button_game9;
    FloatingActionButton hint_button_game10;
    FloatingActionButton hint_button_game11;
    FloatingActionButton hint_button_game12;
    FloatingActionButton hint_button_game13;
    FloatingActionButton hint_button_game14;
    FloatingActionButton hint_button_game15;
    FloatingActionButton hint_button_game16;
    FloatingActionButton hint_button_game17;
    FloatingActionButton hint_button_game18;
    FloatingActionButton hint_button_game19;
    FloatingActionButton hint_button_game20;
    FloatingActionButton hint_button_game21;
    FloatingActionButton hint_button_game22;
    FloatingActionButton hint_button_game23;
    FloatingActionButton hint_button_game24;
    FloatingActionButton hint_button_game25;
    FloatingActionButton hint_button_game26;
    FloatingActionButton hint_button_game27;
    FloatingActionButton hint_button_game28;
    FloatingActionButton hint_button_game29;
    FloatingActionButton hint_button_game30;
    FloatingActionButton hint_button_game31;
    FloatingActionButton hint_button_game32;

    private int currentScore = 0;

    int[] selectedColors;

    @Override
    public void onColorSelected(int color) {
    }

    public class BackgroundSound extends AsyncTask<Void, Void, Void>
    {
        @Override
        protected Void doInBackground(Void... params)
        {
            MediaPlayer player = MediaPlayer.create(GameActivity.this, R.raw.audio);
            player.setLooping(true); // Set looping
            player.setVolume(1.0f, 1.0f);
            player.start();

            return null;
        }
    }

    private FloatingActionButton button_navSettingsOnMain;
    private FloatingActionButton button_navInfoOnMain;
    private TextView textView_highScore;

    public BackgroundSound backgroundSound = new BackgroundSound();

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        //set background music


        //get the highscore
        textView_highScore = findViewById(R.id.textView_highScore);
        textView_highScore.setText(String.valueOf(Model.getInstance().getHighscore()));

        //get the navigation buttons
        button_navSettingsOnMain = findViewById(R.id.button_navSettingsOnMain);
        button_navInfoOnMain = findViewById(R.id.button_navInfoOnMain);

        int[] colors = new int[] {
                Color.parseColor("#00FF00"),
                Color.parseColor("#FF0000"),
                Color.parseColor("#0000FF"),
                Color.parseColor("#FFFF00"),
                Color.parseColor("#FFA500"),
                Color.parseColor("#800080")
        };

        selectedColors = new int[4];

        Random rand = new Random();

        for (int i = 0; i < 4; i++) {
            int randomIndex = rand.nextInt(colors.length);
            int color = colors[randomIndex];
            boolean matches = false;

            if(!Model.getInstance().getHardMode())
            {
                for(int currentColor : selectedColors)
                {
                    Log.i("[HELP]", "Matches?: " + color + " == " + currentColor);
                    if(color == currentColor)
                    {
                        matches = true;
                        break;
                    }
                }
            }
            else
            {
                Log.i("[HELP]", "[WARNING] Hardmode is active!");
            }

            if(!matches)
            {
                selectedColors[i] = color;
                Log.i("[HELP]", String.valueOf(selectedColors[i]));
                Log.i("[HELP]", "-----------------------");
            }
            else
            {
                i--;
            }
        }

        //get the game buttons
        button_game1 = findViewById(R.id.button_game1);
        button_game2 = findViewById(R.id.button_game2);
        button_game3 = findViewById(R.id.button_game3);
        button_game4 = findViewById(R.id.button_game4);
        button_game5 = findViewById(R.id.button_game5);
        button_game6 = findViewById(R.id.button_game6);
        button_game7 = findViewById(R.id.button_game7);
        button_game8 = findViewById(R.id.button_game8);
        button_game9 = findViewById(R.id.button_game9);
        button_game10 = findViewById(R.id.button_game10);
        button_game11 = findViewById(R.id.button_game11);
        button_game12 = findViewById(R.id.button_game12);
        button_game13 = findViewById(R.id.button_game13);
        button_game14 = findViewById(R.id.button_game14);
        button_game15 = findViewById(R.id.button_game15);
        button_game16 = findViewById(R.id.button_game16);
        button_game17 = findViewById(R.id.button_game17);
        button_game18 = findViewById(R.id.button_game18);
        button_game19 = findViewById(R.id.button_game19);
        button_game20 = findViewById(R.id.button_game20);
        button_game21 = findViewById(R.id.button_game21);
        button_game22 = findViewById(R.id.button_game22);
        button_game23 = findViewById(R.id.button_game23);
        button_game24 = findViewById(R.id.button_game24);
        button_game25 = findViewById(R.id.button_game25);
        button_game26 = findViewById(R.id.button_game26);
        button_game27 = findViewById(R.id.button_game27);
        button_game28 = findViewById(R.id.button_game28);
        button_game29 = findViewById(R.id.button_game29);
        button_game30 = findViewById(R.id.button_game30);
        button_game31 = findViewById(R.id.button_game31);
        button_game32 = findViewById(R.id.button_game32);

        hint_button_game1 = findViewById(R.id.hint_game_button1);
        hint_button_game2 = findViewById(R.id.hint_game_button2);
        hint_button_game3 = findViewById(R.id.hint_game_button3);
        hint_button_game4 = findViewById(R.id.hint_game_button18);
        hint_button_game5 = findViewById(R.id.hint_game_button4);
        hint_button_game6 = findViewById(R.id.hint_game_button19);
        hint_button_game7 = findViewById(R.id.hint_game_button5);
        hint_button_game8 = findViewById(R.id.hint_game_button20);
        hint_button_game9 = findViewById(R.id.hint_game_button6);
        hint_button_game10 = findViewById(R.id.hint_game_button21);
        hint_button_game11 = findViewById(R.id.hint_game_button7);
        hint_button_game12 = findViewById(R.id.hint_game_button22);
        hint_button_game13 = findViewById(R.id.hint_game_button8);
        hint_button_game14 = findViewById(R.id.hint_game_button23);
        hint_button_game15 = findViewById(R.id.hint_game_button9);
        hint_button_game16 = findViewById(R.id.hint_game_button24);
        hint_button_game17 = findViewById(R.id.hint_game_button10);
        hint_button_game18 = findViewById(R.id.hint_game_button25);
        hint_button_game19 = findViewById(R.id.hint_game_button11);
        hint_button_game20 = findViewById(R.id.hint_game_button26);
        hint_button_game21 = findViewById(R.id.hint_game_button12);
        hint_button_game22 = findViewById(R.id.hint_game_button27);
        hint_button_game23 = findViewById(R.id.hint_game_button13);
        hint_button_game24 = findViewById(R.id.hint_game_button28);
        hint_button_game25 = findViewById(R.id.hint_game_button14);
        hint_button_game26 = findViewById(R.id.hint_game_button29);
        hint_button_game27 = findViewById(R.id.hint_game_button15);
        hint_button_game28 = findViewById(R.id.hint_game_button30);
        hint_button_game29 = findViewById(R.id.hint_game_button16);
        hint_button_game30 = findViewById(R.id.hint_game_button31);
        hint_button_game31 = findViewById(R.id.hint_game_button17);
        hint_button_game32 = findViewById(R.id.hint_game_button32);

        //disable all buttons
        button_game5.setEnabled(false);
        button_game6.setEnabled(false);
        button_game7.setEnabled(false);
        button_game8.setEnabled(false);
        button_game9.setEnabled(false);
        button_game10.setEnabled(false);
        button_game11.setEnabled(false);
        button_game12.setEnabled(false);
        button_game13.setEnabled(false);
        button_game14.setEnabled(false);
        button_game15.setEnabled(false);
        button_game16.setEnabled(false);
        button_game17.setEnabled(false);
        button_game18.setEnabled(false);
        button_game19.setEnabled(false);
        button_game20.setEnabled(false);
        button_game21.setEnabled(false);
        button_game22.setEnabled(false);
        button_game23.setEnabled(false);
        button_game24.setEnabled(false);
        button_game25.setEnabled(false);
        button_game26.setEnabled(false);
        button_game27.setEnabled(false);
        button_game28.setEnabled(false);
        button_game29.setEnabled(false);
        button_game30.setEnabled(false);
        button_game31.setEnabled(false);
        button_game32.setEnabled(false);


        //add event handler to game buttons
        button_game1.setOnClickListener(this::changeColour);
        button_game2.setOnClickListener(this::changeColour);
        button_game3.setOnClickListener(this::changeColour);
        button_game4.setOnClickListener(this::changeColour);
        button_game5.setOnClickListener(this::changeColour);
        button_game6.setOnClickListener(this::changeColour);
        button_game7.setOnClickListener(this::changeColour);
        button_game8.setOnClickListener(this::changeColour);
        button_game9.setOnClickListener(this::changeColour);
        button_game10.setOnClickListener(this::changeColour);
        button_game11.setOnClickListener(this::changeColour);
        button_game12.setOnClickListener(this::changeColour);
        button_game13.setOnClickListener(this::changeColour);
        button_game14.setOnClickListener(this::changeColour);
        button_game15.setOnClickListener(this::changeColour);
        button_game16.setOnClickListener(this::changeColour);
        button_game17.setOnClickListener(this::changeColour);
        button_game18.setOnClickListener(this::changeColour);
        button_game19.setOnClickListener(this::changeColour);
        button_game20.setOnClickListener(this::changeColour);
        button_game21.setOnClickListener(this::changeColour);
        button_game22.setOnClickListener(this::changeColour);
        button_game23.setOnClickListener(this::changeColour);
        button_game24.setOnClickListener(this::changeColour);
        button_game25.setOnClickListener(this::changeColour);
        button_game26.setOnClickListener(this::changeColour);
        button_game27.setOnClickListener(this::changeColour);
        button_game28.setOnClickListener(this::changeColour);
        button_game29.setOnClickListener(this::changeColour);
        button_game30.setOnClickListener(this::changeColour);
        button_game31.setOnClickListener(this::changeColour);
        button_game32.setOnClickListener(this::changeColour);

        show_infoPage();
        show_settingsPage();

        //show highscore
        textView_highScore.setText(String.valueOf(Model.getInstance().getHighscore()));
    }

    private void changeColour(View view) {
        ColorPickerFragment colorPickerFragment = new ColorPickerFragment();
        ColorStateList colorStateList = ColorStateList.valueOf(Color.BLACK);
        ColorStateList colorStateList1 = ColorStateList.valueOf(Color.WHITE);

        colorPickerFragment.setOnColorSelectedListener(new ColorPickerFragment.OnColorSelectedListener() {
            @Override
            public void onColorSelected(int color) {
                if (view.getId() == R.id.button_game1) {
                    button_game1.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game1.setEnabled(false);
                    check1Row();
                    if (button_game1.getBackgroundTintList().getDefaultColor() == selectedColors[0]) {
                        Log.i("[HELP]", "true");
                        hint_button_game1.setBackgroundTintList(colorStateList);
                    }else if (button_game1.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game1.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game1.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game1.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game2) {
                    button_game2.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game2.setEnabled(false);
                    check1Row();
                    if (button_game2.getBackgroundTintList().getDefaultColor() == selectedColors[1]) {
                        Log.i("[HELP]", "true");
                        hint_button_game2.setBackgroundTintList(colorStateList);
                    }else if (button_game2.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game2.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game2.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game2.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game3) {
                    button_game3.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game3.setEnabled(false);
                    check1Row();
                    if (button_game3.getBackgroundTintList().getDefaultColor() == selectedColors[2]) {
                        Log.i("[HELP]", "true");
                        hint_button_game3.setBackgroundTintList(colorStateList);
                    }else if (button_game3.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game3.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game3.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game3.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game4) {
                    button_game4.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game4.setEnabled(false);
                    check1Row();
                    if (button_game4.getBackgroundTintList().getDefaultColor() == selectedColors[3]) {
                        Log.i("[HELP]", "true");
                        hint_button_game4.setBackgroundTintList(colorStateList);
                    }else if (button_game4.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game4.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game4.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game4.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game5) {
                    button_game5.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game5.setEnabled(false);
                    check2Row();
                    if (button_game5.getBackgroundTintList().getDefaultColor() == selectedColors[0]) {
                        Log.i("[HELP]", "true");
                        hint_button_game5.setBackgroundTintList(colorStateList);
                    }else if (button_game5.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game5.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game5.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game5.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game6) {
                    button_game6.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game6.setEnabled(false);
                    check2Row();
                    if (button_game6.getBackgroundTintList().getDefaultColor() == selectedColors[1]) {
                        Log.i("[HELP]", "true");
                        hint_button_game6.setBackgroundTintList(colorStateList);
                    }else if (button_game6.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game6.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game6.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game6.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game7) {
                    button_game7.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game7.setEnabled(false);
                    check2Row();
                    if (button_game7.getBackgroundTintList().getDefaultColor() == selectedColors[2]) {
                        Log.i("[HELP]", "true");
                        hint_button_game7.setBackgroundTintList(colorStateList);
                    }else if (button_game7.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game7.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game7.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game7.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game8) {
                    button_game8.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game8.setEnabled(false);
                    check2Row();
                    if (button_game8.getBackgroundTintList().getDefaultColor() == selectedColors[3]) {
                        Log.i("[HELP]", "true");
                        hint_button_game8.setBackgroundTintList(colorStateList);
                    }else if (button_game8.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game8.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game8.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game8.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game9) {
                    button_game9.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game9.setEnabled(false);
                    check3Row();
                    if (button_game9.getBackgroundTintList().getDefaultColor() == selectedColors[0]) {
                        Log.i("[HELP]", "true");
                        hint_button_game9.setBackgroundTintList(colorStateList);
                    }else if (button_game9.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game9.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game9.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game9.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game10) {
                    button_game10.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game10.setEnabled(false);
                    check3Row();
                    if (button_game10.getBackgroundTintList().getDefaultColor() == selectedColors[1]) {
                        Log.i("[HELP]", "true");
                        hint_button_game10.setBackgroundTintList(colorStateList);
                    }else if (button_game10.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game10.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game10.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game10.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game11) {
                    button_game11.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game11.setEnabled(false);
                    check3Row();
                    if (button_game11.getBackgroundTintList().getDefaultColor() == selectedColors[2]) {
                        Log.i("[HELP]", "true");
                        hint_button_game11.setBackgroundTintList(colorStateList);
                    }else if (button_game11.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game11.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game11.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game11.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game12) {
                    button_game12.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game12.setEnabled(false);
                    check3Row();
                    if (button_game12.getBackgroundTintList().getDefaultColor() == selectedColors[3]) {
                        Log.i("[HELP]", "true");
                        hint_button_game12.setBackgroundTintList(colorStateList);
                    }else if (button_game12.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game12.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game12.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game12.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game13) {
                    button_game13.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game13.setEnabled(false);
                    check4Row();
                    if (button_game13.getBackgroundTintList().getDefaultColor() == selectedColors[0]) {
                        Log.i("[HELP]", "true");
                        hint_button_game13.setBackgroundTintList(colorStateList);
                    }else if (button_game13.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game13.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game13.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game13.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game14) {
                    button_game14.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game14.setEnabled(false);
                    check4Row();
                    if (button_game14.getBackgroundTintList().getDefaultColor() == selectedColors[1]) {
                        Log.i("[HELP]", "true");
                        hint_button_game14.setBackgroundTintList(colorStateList);
                    }else if (button_game14.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game14.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game14.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game14.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game15) {
                    button_game15.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game15.setEnabled(false);
                    check4Row();
                    if (button_game15.getBackgroundTintList().getDefaultColor() == selectedColors[2]) {
                        Log.i("[HELP]", "true");
                        hint_button_game15.setBackgroundTintList(colorStateList);
                    }else if (button_game15.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game15.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game15.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game15.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game16) {
                    button_game16.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game16.setEnabled(false);
                    check4Row();
                    if (button_game16.getBackgroundTintList().getDefaultColor() == selectedColors[3]) {
                        Log.i("[HELP]", "true");
                        hint_button_game16.setBackgroundTintList(colorStateList);
                    }else if (button_game16.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game16.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game16.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game16.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game17) {
                    button_game17.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game17.setEnabled(false);
                    check5Row();
                    if (button_game17.getBackgroundTintList().getDefaultColor() == selectedColors[0]) {
                        Log.i("[HELP]", "true");
                        hint_button_game17.setBackgroundTintList(colorStateList);
                    }else if (button_game17.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game17.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game17.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game17.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game18) {
                    button_game18.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game18.setEnabled(false);
                    check5Row();
                    if (button_game18.getBackgroundTintList().getDefaultColor() == selectedColors[1]) {
                        Log.i("[HELP]", "true");
                        hint_button_game18.setBackgroundTintList(colorStateList);
                    }else if (button_game18.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game18.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game18.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game18.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game19) {
                    button_game19.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game19.setEnabled(false);
                    check5Row();
                    if (button_game19.getBackgroundTintList().getDefaultColor() == selectedColors[2]) {
                        Log.i("[HELP]", "true");
                        hint_button_game19.setBackgroundTintList(colorStateList);
                    }else if (button_game19.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game19.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game19.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game19.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game20) {
                    button_game20.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game20.setEnabled(false);
                    check5Row();
                    if (button_game20.getBackgroundTintList().getDefaultColor() == selectedColors[3]) {
                        Log.i("[HELP]", "true");
                        hint_button_game20.setBackgroundTintList(colorStateList);
                    }else if (button_game20.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game20.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game20.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game20.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game21) {
                    button_game21.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game21.setEnabled(false);
                    check6Row();
                    if (button_game21.getBackgroundTintList().getDefaultColor() == selectedColors[0]) {
                        Log.i("[HELP]", "true");
                        hint_button_game21.setBackgroundTintList(colorStateList);
                    }else if (button_game21.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game21.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game21.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game21.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game22) {
                    button_game22.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game22.setEnabled(false);
                    check6Row();
                    if (button_game22.getBackgroundTintList().getDefaultColor() == selectedColors[1]) {
                        Log.i("[HELP]", "true");
                        hint_button_game22.setBackgroundTintList(colorStateList);
                    }else if (button_game22.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game22.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game22.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game22.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game23) {
                    button_game23.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game23.setEnabled(false);
                    check6Row();
                    if (button_game23.getBackgroundTintList().getDefaultColor() == selectedColors[2]) {
                        Log.i("[HELP]", "true");
                        hint_button_game23.setBackgroundTintList(colorStateList);
                    }else if (button_game23.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game23.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game23.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game23.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game24) {
                    button_game24.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game24.setEnabled(false);
                    check6Row();
                    if (button_game24.getBackgroundTintList().getDefaultColor() == selectedColors[3]) {
                        Log.i("[HELP]", "true");
                        hint_button_game24.setBackgroundTintList(colorStateList);
                    }else if (button_game24.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game24.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game24.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game24.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game25) {
                    button_game25.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game25.setEnabled(false);
                    check7Row();
                    if (button_game25.getBackgroundTintList().getDefaultColor() == selectedColors[0]) {
                        Log.i("[HELP]", "true");
                        hint_button_game25.setBackgroundTintList(colorStateList);
                    }else if (button_game25.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game25.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game25.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game25.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game26) {
                    button_game26.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game26.setEnabled(false);
                    check7Row();
                    if (button_game26.getBackgroundTintList().getDefaultColor() == selectedColors[1]) {
                        Log.i("[HELP]", "true");
                        hint_button_game26.setBackgroundTintList(colorStateList);
                    }else if (button_game26.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game26.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game26.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game26.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game27) {
                    button_game27.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game27.setEnabled(false);
                    check7Row();
                    if (button_game27.getBackgroundTintList().getDefaultColor() == selectedColors[2]) {
                        Log.i("[HELP]", "true");
                        hint_button_game27.setBackgroundTintList(colorStateList);
                    }else if (button_game27.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game27.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game27.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game27.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game28) {
                    button_game28.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game28.setEnabled(false);
                    check7Row();
                    if (button_game28.getBackgroundTintList().getDefaultColor() == selectedColors[3]) {
                        Log.i("[HELP]", "true");
                        hint_button_game28.setBackgroundTintList(colorStateList);
                    }else if (button_game28.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game28.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game28.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game28.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game29) {
                    button_game29.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game29.setEnabled(false);
                    check8Row();
                    if (button_game29.getBackgroundTintList().getDefaultColor() == selectedColors[0]) {
                        Log.i("[HELP]", "true");
                        hint_button_game29.setBackgroundTintList(colorStateList);
                    }else if (button_game29.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game29.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game29.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game29.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game30) {
                    button_game30.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game30.setEnabled(false);
                    check8Row();
                    if (button_game30.getBackgroundTintList().getDefaultColor() == selectedColors[1]) {
                        Log.i("[HELP]", "true");
                        hint_button_game30.setBackgroundTintList(colorStateList);
                    }else if (button_game30.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game30.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game30.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game30.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game31) {
                    button_game31.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game31.setEnabled(false);
                    check8Row();
                    if (button_game31.getBackgroundTintList().getDefaultColor() == selectedColors[2]) {
                        Log.i("[HELP]", "true");
                        hint_button_game31.setBackgroundTintList(colorStateList);
                    }else if (button_game31.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game31.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game31.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game31.setBackgroundTintList(colorStateList1);
                    }
                }
                else if (view.getId() == R.id.button_game32) {
                    button_game32.setBackgroundTintList(ColorStateList.valueOf(color));
                    button_game32.setEnabled(false);
                    check8Row();
                    if (button_game32.getBackgroundTintList().getDefaultColor() == selectedColors[3]) {
                        Log.i("[HELP]", "true");
                        hint_button_game32.setBackgroundTintList(colorStateList);
                    }else if (button_game32.getBackgroundTintList().getDefaultColor() == selectedColors[1] || button_game32.getBackgroundTintList().getDefaultColor() == selectedColors[2] || button_game32.getBackgroundTintList().getDefaultColor() == selectedColors[3])
                    {
                        hint_button_game32.setBackgroundTintList(colorStateList1);
                    }
                }
            }
        });
        colorPickerFragment.show(getSupportFragmentManager(), "ColorPickerFragment");
    }

    private void check1Row(){
            if(button_game1.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191")  && button_game2.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191") && button_game3.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191") && button_game4.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191")) {
                currentScore++;
                if (button_game1.getBackgroundTintList().getDefaultColor() == selectedColors[0] && button_game2.getBackgroundTintList().getDefaultColor() == selectedColors[1] && button_game3.getBackgroundTintList().getDefaultColor() == selectedColors[2] && button_game4.getBackgroundTintList().getDefaultColor() == selectedColors[3]) {
                    show_winningFragment();
                }else
                {
                    button_game5.setEnabled(true);
                    button_game6.setEnabled(true);
                    button_game7.setEnabled(true);
                    button_game8.setEnabled(true);
                }
            }
    }


    private void check2Row(){
        if(button_game5.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191") && button_game6.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191") && button_game7.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191") && button_game8.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191")) {
            currentScore++;
            if (button_game5.getBackgroundTintList().getDefaultColor() == selectedColors[0] && button_game6.getBackgroundTintList().getDefaultColor() == selectedColors[1] && button_game7.getBackgroundTintList().getDefaultColor() == selectedColors[2] && button_game8.getBackgroundTintList().getDefaultColor() == selectedColors[3]) {
                show_winningFragment();
            }else
            {
                button_game9.setEnabled(true);
                button_game10.setEnabled(true);
                button_game11.setEnabled(true);
                button_game12.setEnabled(true);
            }
        }
    }

    private void check3Row(){

        if(button_game9.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191") && button_game10.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191") && button_game11.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191") && button_game12.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191")) {
            currentScore++;
            if (button_game9.getBackgroundTintList().getDefaultColor() == selectedColors[0] && button_game10.getBackgroundTintList().getDefaultColor() == selectedColors[1] && button_game11.getBackgroundTintList().getDefaultColor() == selectedColors[2] && button_game12.getBackgroundTintList().getDefaultColor() == selectedColors[3]) {
                show_winningFragment();
            }else
            {
                button_game13.setEnabled(true);
                button_game14.setEnabled(true);
                button_game15.setEnabled(true);
                button_game16.setEnabled(true);
            }
        }
    }

    private void check4Row(){

        if(button_game13.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191") && button_game14.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191") && button_game15.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191") && button_game16.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191")) {
            currentScore++;
            if (button_game13.getBackgroundTintList().getDefaultColor() == selectedColors[0] && button_game14.getBackgroundTintList().getDefaultColor() == selectedColors[1] && button_game15.getBackgroundTintList().getDefaultColor() == selectedColors[2] && button_game16.getBackgroundTintList().getDefaultColor() == selectedColors[3]) {
                show_winningFragment();
            }else
            {
                button_game17.setEnabled(true);
                button_game18.setEnabled(true);
                button_game19.setEnabled(true);
                button_game20.setEnabled(true);
            }
        }
    }

    private void check5Row(){

        if(button_game17.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191") && button_game18.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191") && button_game19.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191") && button_game20.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191")) {
            currentScore++;
            if (button_game17.getBackgroundTintList().getDefaultColor() == selectedColors[0] && button_game18.getBackgroundTintList().getDefaultColor() == selectedColors[1] && button_game19.getBackgroundTintList().getDefaultColor() == selectedColors[2] && button_game20.getBackgroundTintList().getDefaultColor() == selectedColors[3]) {
                show_winningFragment();
            }else
            {
                button_game21.setEnabled(true);
                button_game22.setEnabled(true);
                button_game23.setEnabled(true);
                button_game24.setEnabled(true);
            }
        }
    }

    private void check6Row(){
        if(button_game21.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191") && button_game22.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191") && button_game23.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191") && button_game24.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191")) {
            currentScore++;
            if (button_game21.getBackgroundTintList().getDefaultColor() == selectedColors[0] && button_game22.getBackgroundTintList().getDefaultColor() == selectedColors[1] && button_game23.getBackgroundTintList().getDefaultColor() == selectedColors[2] && button_game24.getBackgroundTintList().getDefaultColor() == selectedColors[3]) {
                show_winningFragment();
            }else
            {
                button_game25.setEnabled(true);
                button_game26.setEnabled(true);
                button_game27.setEnabled(true);
                button_game28.setEnabled(true);
            }
        }
    }

    private void check7Row(){
        if(button_game25.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191") && button_game26.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191") && button_game27.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191") && button_game28.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191")) {
            currentScore++;
            if (button_game25.getBackgroundTintList().getDefaultColor() == selectedColors[0] && button_game26.getBackgroundTintList().getDefaultColor() == selectedColors[1] && button_game27.getBackgroundTintList().getDefaultColor() == selectedColors[2] && button_game28.getBackgroundTintList().getDefaultColor() == selectedColors[3]) {
                show_winningFragment();
            }else
            {
                button_game29.setEnabled(true);
                button_game30.setEnabled(true);
                button_game31.setEnabled(true);
                button_game32.setEnabled(true);
            }
        }
    }

    private void check8Row(){
        if(button_game29.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191") && button_game30.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191") && button_game31.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191") && button_game32.getBackgroundTintList().getDefaultColor() != Color.parseColor("#919191")) {
            currentScore++;
            if (button_game29.getBackgroundTintList().getDefaultColor() == selectedColors[0] && button_game30.getBackgroundTintList().getDefaultColor() == selectedColors[1] && button_game31.getBackgroundTintList().getDefaultColor() == selectedColors[2] && button_game32.getBackgroundTintList().getDefaultColor() == selectedColors[3]) {
                show_winningFragment();
            }else
            {
                show_losingFragment();
            }
        }
    }

    private void show_winningFragment(){

        Log.i("[HELP]", "CurrentScore: " + currentScore + " -- Database: " + Model.getInstance().getHighscore());
        if(currentScore < Model.getInstance().getHighscore() && Model.getInstance().getHighscore() != 0)
        {
            Model.getInstance().setHighscore(currentScore);
        }
        Model.getInstance().setCurrentScore(currentScore);

        WinningFragment winningFragment = new WinningFragment();
        winningFragment.show(getSupportFragmentManager());

    }

    private void show_losingFragment(){
        LosingFragment losingFragment = new LosingFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("color1", selectedColors[0]);
        bundle.putInt("color2", selectedColors[1]);
        bundle.putInt("color3", selectedColors[2]);
        bundle.putInt("color4", selectedColors[3]);
        losingFragment.setArguments(bundle);

        losingFragment.show(getSupportFragmentManager());
    }

    private void show_settingsPage(){
        button_navSettingsOnMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GameActivity.this, SettingsActivity.class);
                startActivity(intent);
            }
        });
    }

    private void show_infoPage()
    {
        button_navInfoOnMain.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(GameActivity.this, HelpActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onDestroy()
    {
        Model.getInstance().save();
        backgroundSound.cancel(true);
        super.onDestroy();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        backgroundSound.cancel(true);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        Log.i("[HELP]",  ""+ Model.getInstance().getBackgroundMusic());
        if(Model.getInstance().getBackgroundMusic())
        {
            backgroundSound.execute((Void) null);
        }
    }
}

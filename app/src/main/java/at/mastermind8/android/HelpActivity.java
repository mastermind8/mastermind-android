package at.mastermind8.android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class HelpActivity extends AppCompatActivity {
    private FloatingActionButton button_navHomeOnMain;
    private FloatingActionButton button_navSettingsOnMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        button_navHomeOnMain = findViewById(R.id.button_navHomeOnMain);
        button_navSettingsOnMain = findViewById(R.id.button_navSettingsOnMain);

        show_homePage();
        show_settingsPage();
    }

    private void show_settingsPage(){
        button_navSettingsOnMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HelpActivity.this, SettingsActivity.class);
                startActivity(intent);
            }
        });
    }

    private void show_homePage(){
        button_navHomeOnMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HelpActivity.this, GameActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onDestroy()
    {
        Model.getInstance().save();
        super.onDestroy();
    }
}

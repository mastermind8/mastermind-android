package at.mastermind8.android;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class LosingFragment extends DialogFragment {

    FloatingActionButton btn1, btn2, btn3, btn4;

    private int color1;
    private int color2;
    private int color3;
    private int color4;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_losing, container, false);

        Button btn_tryAgain = view.findViewById(R.id.button_tryAgain_defeat);
        btn_tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), GameActivity.class);
                startActivity(intent);
                dismiss();
            }
        });

        Bundle bundle = getArguments();
        assert bundle != null;
        color1 = bundle.getInt("color1");
        Log.i("[HELP]", String.valueOf(color1));
        color2 = bundle.getInt("color2");
        color3 = bundle.getInt("color3");
        color4 = bundle.getInt("color4");


        btn1 = view.findViewById(R.id.btn_correct_color1);
        btn2 = view.findViewById(R.id.btn_correct_color2);
        btn3 = view.findViewById(R.id.btn_correct_color3);
        btn4 = view.findViewById(R.id.btn_correct_color4);

        btn1.setBackgroundTintList(ColorStateList.valueOf(color1));
        btn2.setBackgroundTintList(ColorStateList.valueOf(color2));
        btn3.setBackgroundTintList(ColorStateList.valueOf(color3));
        btn4.setBackgroundTintList(ColorStateList.valueOf(color4));

        return view;
    }

    public void show(FragmentManager fragmentManager) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(this, "LosingFragment");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}

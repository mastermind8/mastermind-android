package at.mastermind8.android;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import androidx.room.Room;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity
{

    private ImageView imgView_help;
    private ImageView imgView_play;
    private ImageView imgView_settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imgView_help = findViewById(R.id.btn_help);
        imgView_play = findViewById(R.id.btn_spiel);
        imgView_settings = findViewById(R.id.btn_settings);

        show_gamePage();
        show_helpPage();
        show_settingsPage();

        Model.getInstance().setDatabase(Room.databaseBuilder(this, SettingsDatabase.class, "at.mastermind8.android.database").allowMainThreadQueries().build());
        Model.getInstance().init();
    }

    private void show_helpPage(){
        imgView_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, HelpActivity.class);
                startActivity(intent);
            }
        });
    }

    private void show_settingsPage(){
        imgView_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Model.getInstance().init();
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent);
            }
        });
    }

    private void show_gamePage(){
        imgView_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, GameActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onDestroy()
    {
        Model.getInstance().save();
        super.onDestroy();
    }
}

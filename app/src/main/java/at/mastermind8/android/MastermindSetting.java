package at.mastermind8.android;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class MastermindSetting
{
    @PrimaryKey
    private int id;

    private boolean backgroundMusic;
    private boolean hardMode;
    private int highscore;

    public MastermindSetting(int id)
    {
        backgroundMusic = true;
        hardMode = true;
        highscore = 0;
        this.id = id;
    }

    public boolean getBackgroundMusic()
    {
        return backgroundMusic;
    }

    public void setBackgroundMusic(boolean backgroundMusic)
    {
        this.backgroundMusic = backgroundMusic;
    }

    public boolean getHardMode()
    {
        return hardMode;
    }

    public void setHardMode(boolean hardMode)
    {
        this.hardMode = hardMode;
    }

    public int getHighscore()
    {
        return highscore;
    }

    public void setHighscore(int highscore)
    {
        this.highscore = highscore;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }
}

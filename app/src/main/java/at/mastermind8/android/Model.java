package at.mastermind8.android;

public class Model
{
    private static Model instance;
    private SettingsDatabase database;
    private MastermindSetting settings;
    private boolean isNew = false;
    private int highscore;
    private int currentScore;

    public static Model getInstance()
    {
        if (instance == null)
        {
            instance = new Model();
        }
        return instance;
    }

    private Model()
    {

    }

    public void init()
    {
        settings = database.settingsDAO().findById(0);
        if(settings == null)
        {
            settings = new MastermindSetting(0);
            isNew = true;
        }
        highscore = settings.getHighscore();
    }

    public void save()
    {
        if(isNew)
        {
            database.settingsDAO().insert(settings);
            isNew = false;
        }
        else
        {
            database.settingsDAO().update(settings);
        }
    }

    public SettingsDatabase getDatabase()
    {
        return database;
    }

    public void setDatabase(SettingsDatabase database)
    {
        this.database = database;
    }

    public boolean getBackgroundMusic()
    {
        return settings.getBackgroundMusic();
    }

    public void setBackgroundMusic(boolean backgroundMusic)
    {
        settings.setBackgroundMusic(backgroundMusic);
        save();
    }

    public boolean getHardMode()
    {
        return settings.getHardMode();
    }

    public void setHardMode(boolean hardMode)
    {
        settings.setHardMode(hardMode);
        save();
    }

    public int getHighscore()
    {
        return settings.getHighscore();
    }

    public void setHighscore(int highscore)
    {
        this.highscore = highscore;
        settings.setHighscore(highscore);
        save();
    }

    public int getCurrentScore() {
        return currentScore;
    }

    public void setCurrentScore(int currentScore) {
        this.currentScore = currentScore;
    }
}

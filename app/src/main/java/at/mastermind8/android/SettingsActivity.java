package at.mastermind8.android;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.RadioButton;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class SettingsActivity extends AppCompatActivity {
    private FloatingActionButton button_navHomeOnMain;
    private FloatingActionButton button_navInfoOnMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        button_navHomeOnMain = findViewById(R.id.button_navHomeOnMain);
        button_navInfoOnMain = findViewById(R.id.button_navInfoOnMain);
        show_helpPage();
        show_homePage();

        RadioButton button_MusicOFF = findViewById(R.id.button_MusicOFF);
        RadioButton button_MusicON = findViewById(R.id.button_MusicON);
        RadioButton button_gamemodeEASY = findViewById(R.id.button_gamemodeEASY);
        RadioButton button_gamemodeHARD = findViewById(R.id.button_gamemodeHARD);
        Button button_resetHighscore = findViewById(R.id.button_resetHighscore);

        button_MusicOFF.setChecked(!Model.getInstance().getBackgroundMusic());
        button_MusicON.setChecked(Model.getInstance().getBackgroundMusic());
        button_gamemodeEASY.setChecked(!Model.getInstance().getHardMode());
        button_gamemodeHARD.setChecked(Model.getInstance().getHardMode());
        Log.i("[HELP]", "Hardmod is " + Model.getInstance().getHardMode());

        button_MusicOFF.setOnClickListener(v ->
        {
            if(((RadioButton) v).isChecked())
            {
                Model.getInstance().setBackgroundMusic(false);
                //GameActivity.backgroundSound.cancel(true);
            }
        });

        button_MusicON.setOnClickListener(v ->
        {
            if(((RadioButton) v).isChecked())
            {
                Model.getInstance().setBackgroundMusic(true);
            }
        });

        button_gamemodeEASY.setOnClickListener(v ->
        {
            if(((RadioButton) v).isChecked())
            {
                Model.getInstance().setHardMode(false);
            }
        });

        button_gamemodeHARD.setOnClickListener(v ->
        {
            if(((RadioButton) v).isChecked())
            {
                Model.getInstance().setHardMode(true);
            }
        });

        button_resetHighscore.setOnClickListener(v -> Model.getInstance().setHighscore(0));
    }

    private void show_homePage() {
        button_navHomeOnMain.setOnClickListener(view ->
        {
            Intent intent = new Intent(SettingsActivity.this, GameActivity.class);
            startActivity(intent);
        });
    }

    private void show_helpPage() {
        button_navInfoOnMain.setOnClickListener(view ->
        {
            Intent intent = new Intent(SettingsActivity.this, HelpActivity.class);
            startActivity(intent);
        });
    }

    @Override
    public void onDestroy()
    {
        Model.getInstance().save();
        super.onDestroy();
    }
}

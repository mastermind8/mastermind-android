package at.mastermind8.android;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface SettingsDAO
{
    @Query("SELECT * FROM mastermindsetting")
    List<MastermindSetting> getAllItems();

    @Query("SELECT * FROM mastermindsetting WHERE id = :itemId")
    MastermindSetting findById(int itemId);

    @Insert
    void insert(MastermindSetting settingsItem);

    @Update
    void update(MastermindSetting settingsItem);

    @Delete
    void deleteAll(MastermindSetting settingsItem);
}

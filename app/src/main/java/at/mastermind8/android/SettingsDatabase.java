package at.mastermind8.android;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {MastermindSetting.class}, version = 1, exportSchema = false)
public abstract class SettingsDatabase extends RoomDatabase
{
    public abstract SettingsDAO settingsDAO();
}
